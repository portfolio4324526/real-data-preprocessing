# -*- coding: utf-8 -*-

# Real data processing project

# Importing necessary libraries
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import json

df = pd.read_csv("EURUSD_H4.csv")  # Reading the CSV file

df_first_2500 = df.head(2500)  # Selecting the first 2500 elements

df_cleaned = df_first_2500.drop(columns=["SMA14IND", "SMA50IND"])  # Dropping columns

num_missing_values = df_cleaned['Close'].isna().sum()
# Counting missing values in the 'Close' column, the result is 26

def fill_missing_with_neighbors_mean(df, column):  # Filling missing values with the average of neighboring elements
    for i in range(1, len(df) - 1):
        if pd.isna(df.loc[i, column]):
            df.loc[i, column] = (df.loc[i - 1, column] + df.loc[i + 1, column]) / 2
    return df

df_cleaned = fill_missing_with_neighbors_mean(df_cleaned, 'Close')  # Filling missing values

num_missing_values_after = df_cleaned['Close'].isna().sum()
print(f"Number of missing values in the 'Close' column after filling: {num_missing_values_after}")  # Checking if there are any missing values left

num_missing_values_SMA14 = df_cleaned['SMA14'].isna().sum()  # Counting missing values in SMA14 and SMA50
num_missing_values_SMA50 = df_cleaned['SMA50'].isna().sum()
print(f"Number of missing values in the 'SMA14' column: {num_missing_values_SMA14}")
print(f"Number of missing values in the 'SMA50' column: {num_missing_values_SMA50}")

df_cleaned['SMA14'].fillna(df_cleaned['SMA14'].mean(), inplace=True)  # Filling missing values with the mean of the column
df_cleaned['SMA50'].fillna(df_cleaned['SMA50'].mean(), inplace=True)

num_missing_values_SMA14_after = df_cleaned['SMA14'].isna().sum()  # Checking if there are any missing values left
num_missing_values_SMA50_after = df_cleaned['SMA50'].isna().sum()
print(f"Number of missing values in the 'SMA14' column after filling: {num_missing_values_SMA14_after}")
print(f"Number of missing values in the 'SMA50' column after filling: {num_missing_values_SMA50_after}")

columns_to_fill_zero = ["Bulls", "CCI", "DM", "OSMA", "RSI", "Stoch"]
df_cleaned[columns_to_fill_zero] = df_cleaned[columns_to_fill_zero].fillna(0)

for column in columns_to_fill_zero:
    num_missing_values = df_cleaned[column].isna().sum()
    print(f"Number of missing values in the '{column}' column after filling: {num_missing_values}")

# Correlation between columns "SMA14" and "SMA50"
correlation_SMA14_SMA50 = df_cleaned[['SMA14', 'SMA50']].corr().iloc[0, 1]
print(f"Correlation between columns 'SMA14' and 'SMA50': {correlation_SMA14_SMA50}")

# Determining the correlation between "Close" and "SMA14" and between "Close" and "SMA50"
correlation_close_SMA14 = df_cleaned[['Close', 'SMA14']].corr().iloc[0, 1]
correlation_close_SMA50 = df_cleaned[['Close', 'SMA50']].corr().iloc[0, 1]
print(f"Correlation between column 'Close' and 'SMA14': {correlation_close_SMA14}")
print(f"Correlation between column 'Close' and 'SMA50': {correlation_close_SMA50}")

# Dropping the column with a higher correlation with the "Close" column
if correlation_close_SMA14 > correlation_close_SMA50:
    df_cleaned.drop(columns=['SMA14'], inplace=True)
    print("Dropped column 'SMA14'")
else:
    df_cleaned.drop(columns=['SMA50'], inplace=True)
    print("Dropped column 'SMA50'")

num_negative_CCI = (df_cleaned['CCI'] < 0).sum()
print(f"Number of negative elements for the 'CCI' attribute: {num_negative_CCI}")

# Maximum and minimum value for each remaining attribute
max_values = df_cleaned[["Close", "SMA50", "Bulls", "CCI", "DM", "OSMA", "RSI", "Stoch"]].max()
min_values = df_cleaned[["Close", "SMA50", "Bulls", "CCI", "DM", "OSMA", "RSI", "Stoch"]].min()

# Displaying maximum values for each attribute
print("Maximum values:")
print(max_values)

# Displaying minimum values for each attribute
print("\nMinimum values:")
print(min_values)

# Attributes to normalize
attributes_to_normalize = ["Close", "SMA50"]

# MinMaxScaler object
scaler = MinMaxScaler()

# Normalizing selected attributes
df_normalized = df_cleaned.copy()
df_normalized[attributes_to_normalize] = scaler.fit_transform(df_normalized[attributes_to_normalize])

# Modified DataFrame with normalized attributes
print(df_normalized)

# Discretization into 2 categories
df_normalized['Close_2_categories'] = pd.cut(df_normalized['Bulls'], 2, labels=["Low", "High"])
df_normalized['SMA50_2_categories'] = pd.cut(df_normalized['CCI'], 2, labels=["Low", "High"])

# Discretization into 4 categories
df_normalized['Close_4_categories'] = pd.qcut(df_normalized['Bulls'], q=4, labels=["Very Low", "Low", "Medium", "High"])
df_normalized['SMA50_4_categories'] = pd.qcut(df_normalized['CCI'], q=4, labels=["Very Low", "Low", "Medium", "High"])

# Displaying the modified DataFrame
print(df_normalized)

# Counting occurrences of each decision
decision_counts = df['Decision'].value_counts()

# Creating a pie chart
decision_counts = df_normalized['Decision'].value_counts()
plt.figure(figsize=(8, 6))
plt.pie(decision_counts, labels=decision_counts.index, autopct='%1.1f%%', startangle=140)
plt.title('Decision Distribution')
plt.axis('equal')
plt.show()


# Creating a line chart
plt.figure(figsize=(8, 6))
plt.plot(df_normalized['Close'], color='red')
plt.title('Close Attribute Variability')
plt.xlabel('Change')
plt.ylabel('Close Value')
plt.grid(True)
plt.show()

df_cleaned.to_json('cleaned_dataframe.json')

df_cleaned

df_cleaned