# Real data preprocessing

This project processes real financial data, performing various data preprocessing steps and visualizations.

## Setup

1. Clone the repository:

   git clone https://gitlab.com/portfolio4324526/real-data-preprocessing
   
## Navigate to the project directory
	
	cd https://gitlab.com/portfolio4324526/real-data-preprocessing
	
## Install required libraries 

	pip install pandas matplotlib scikit-learn json
	
## Ensure the EURUSD_H4.csv file is in the project directory

## Script Overview

	Loading Data: Loads the first 2500 rows from EURUSD_H4.csv.
	Data Cleaning: Handles missing values and removes unnecessary columns.
	Normalization: Normalizes selected attributes.
	Discretization: Discretizes selected attributes.
	Visualization: Generates pie and line charts.
	Saving Data: Saves the cleaned data to a JSON file.
	
## Visualizations 
	
	Pie Chart: Distribution of decision values.
	Line Chart: Variability of the Close attribute.